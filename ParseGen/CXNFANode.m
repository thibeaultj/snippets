//
//  CXNFANode.m
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import "CXNFANode.h"
#import "CXTerminal.h"
#import "CXConflictOptions.h"

@implementation CXNFANode
#pragma mark Support Methods
- (void)eClosureWithVisited:(NSMutableSet *)visited {
	if (![visited containsObject:self]) {
		[visited addObject:self];
		for (CXNFANode *node in [_transitions objectForKey:[NSNumber numberWithUnsignedInteger:CXTransitionEmpty]])
			[node eClosureWithVisited:visited];
	}
}

#pragma mark Initialization/Deallocation Methods
- (id)init {
	if (self = [super init]) {
		_transitions = [[NSMutableDictionary alloc] init];
		_acceptPriority = CXPriorityMinimum;
	}
	return self;
}
- (void)dealloc {
	[_transitions release];
	[_acceptValue release];
	[_eClosure release];
	[super dealloc];
}

#pragma mark Property Methods
- (NSDictionary *)transitions {
	return _transitions;
}
- (id)acceptValue {
	return _acceptValue;
}
- (void)setAcceptValue:(id)value {
	[value retain];
	[_acceptValue release];
	_acceptValue = value;
}
- (NSInteger)acceptPriority {
	return _acceptPriority;
}
- (void)setAcceptPriority:(NSInteger)priority {
	_acceptPriority = priority;
}
- (NSSet *)eClosure {
	if (!_eClosure) {
		_eClosure = [[NSMutableSet alloc] init];
		[self eClosureWithVisited:_eClosure];
	}
	return _eClosure;
}

#pragma mark Derived Property Methods
- (NSSet *)inputs {
	return [NSSet setWithArray:[_transitions allKeys]];
}

#pragma mark Utility Methods
- (void)addTransition:(NSUInteger)input toNode:(CXNFANode *)node {
	NSNumber *inputKey = [NSNumber numberWithUnsignedInteger:input];
	NSMutableSet *nodes = [_transitions objectForKey:inputKey];
	if (!nodes) {
		nodes = [NSMutableSet set];
		[_transitions setObject:nodes forKey:inputKey];
	}
	[nodes addObject:node];
}

#pragma mark NSCopying Protocol Methods
- (id)copyWithZone:(NSZone *)zone {
	return [self retain];
}
@end
