//
//  CXDFAPartition.m
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import "CXDFAPartition.h"
#import "CXDFANode.h"
#import "CXDFAPartitionGroup.h"

@implementation CXDFAPartition
#pragma mark Initialization/Deallocation Methods
- (id)init {
	if (self = [super init]) {
		_groups = [[NSMutableSet alloc] init];
		_nodeGroups = [[NSMutableDictionary alloc] init];
	}
	return self;
}
- (void)dealloc {
	[_groups release];
	[_nodeGroups release];
	[super dealloc];
}

#pragma mark Property Methods
- (NSSet *)groups {
	return _groups;
}

#pragma mark Utility Methods
- (void)addGroup:(CXDFAPartitionGroup *)group {
	[_groups addObject:group];
	for (CXDFANode *node in [group nodes]) 
		[_nodeGroups setObject:group forKey:node];
}
- (CXDFAPartitionGroup *)groupForNode:(CXDFANode *)node {
	return [_nodeGroups objectForKey:node];
}
- (BOOL)canAddNode:(CXDFANode *)node toSubgroup:(CXDFAPartitionGroup *)group {
	if (![[node inputs] isEqual:[group inputs]])
		return NO;
	CXDFANode *rep = [[group nodes] anyObject];
	for (NSNumber *i in [rep inputs]) {
		NSUInteger input = [i unsignedIntegerValue];
		CXDFANode *target = [rep nodeForInput:input];
		CXDFANode *check = [node nodeForInput:input];
		CXDFAPartitionGroup *targetGroup = [_nodeGroups objectForKey:target];
		CXDFAPartitionGroup *checkGroup = [_nodeGroups objectForKey:check];
		if (![checkGroup isEqual:targetGroup])
			return NO;
	}
	return YES;
}
- (CXDFAPartition *)split {
	CXDFAPartition *split = [CXDFAPartition partition];
	for (CXDFAPartitionGroup *group in _groups) {
		if ([group count] == 0) continue;
		else if ([group count] == 1) [split addGroup:group];
		else {
			NSMutableSet *subgroups = [NSMutableSet set];
			for (CXDFANode *node in [group nodes]) {
				BOOL added = NO;
				for (CXDFAPartitionGroup *subgroup in subgroups) {
					if ([self canAddNode:node toSubgroup:subgroup]) {
						[subgroup addNode:node];
						added = YES;
						break;
					}
				}
				if (!added){
					CXDFAPartitionGroup *pg = [[CXDFAPartitionGroup alloc] init];
					[pg addNode:node];
					[subgroups addObject:pg];
					[pg release];
				}
			}
			if ([subgroups count] == 1)
				[split addGroup:group];
			else {
				for (CXDFAPartitionGroup *sg in subgroups)
					[split addGroup:sg];
			}
		}
	}
	if ([[split groups] isEqualToSet:_groups])
		return self;
	return split;
}

#pragma mark Class Methods
+ (CXDFAPartition *)partition {
	return [[[CXDFAPartition alloc] init] autorelease];
}
@end
