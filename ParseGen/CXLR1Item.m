//
//  CXLR1Item.m
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import "CXLR1Item.h"
#import "CXLR0Item.h"
#import "CXTerminal.h"
#import "CXSymbol.h"
#import "CXLR1ItemSet.h"
#import "CXRule.h"

@implementation CXLR1Item
#pragma mark Initialization/Deallocation Methods
- (id)initWithLR0Item:(CXLR0Item *)item terminal:(CXTerminal *)terminal {
	if (self = [super init]) {
		_lr0 = [item retain];
		_terminal = [terminal retain];
	}
	return self;
}
- (void)dealloc {
	[_lr0 release];
	[_terminal release];
	[super dealloc];
}

#pragma mark Property Methods
- (CXLR0Item *)lr0 {
	return _lr0;
}
- (CXTerminal *)terminal {
	return _terminal;
}

#pragma mark Derived Property Methods
- (CXSymbol *)elementAtMarker {
	return [_lr0 elementAtMarker];
}
- (CXSymbol *)elementAtMarkerOffset:(NSUInteger)offset {
	return [_lr0 elementAtMarkerOffset:offset];
}

#pragma mark Utility Methods
- (void)closureWithSet:(CXLR1ItemSet *)set {
	[set addItem:self];
	
	CXSymbol *B = [self elementAtMarker];
	if ([B isKindOfClass:[CXRule class]]) {
		CXSymbol *beta = [self elementAtMarkerOffset:1];
		NSSet *first = [beta firstWithSymbol:_terminal];
		for (CXProduction *gamma in [(CXRule *)B productions]) {
			for (CXTerminal *b in first) {
				CXLR0Item *lr0 = [CXLR0Item itemWithProduction:gamma marker:0];
				CXLR1Item *lr1 = [CXLR1Item itemWithLR0:lr0 terminal:b];
				if (![set containsItem:lr1])
					[lr1 closureWithSet:set];
			}
		}
	}
}
- (CXLR1ItemSet *)closure {
	CXLR1ItemSet *set = [CXLR1ItemSet set];
	[self closureWithSet:set];
	return set;
}

#pragma mark Class Methods
+ (CXLR1Item *)itemWithLR0:(CXLR0Item *)item terminal:(CXTerminal *)terminal {
	return [[[CXLR1Item alloc] initWithLR0Item:item terminal:terminal] autorelease];
}

#pragma mark NSCopying Protocol Methods
- (id)copyWithZone:(NSZone *)zone {
	return [self retain];
}

#pragma mark NSObject Overrides
- (NSUInteger)hash {
	NSUInteger h = 0;
	h = h * 37 + [_lr0 hash];
	h = h * 37 + [_terminal hash];
	return h;
}
- (BOOL)isEqual:(id)object {
	if ([object isKindOfClass:[CXLR1Item class]])
		return [self hash] == [object hash];
	return NO;
}
@end
