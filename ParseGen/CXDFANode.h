//
//  CXDFANode.h
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CXDFANode : NSObject <NSCopying> {
	NSSet *_nfaNodes;
	NSMutableDictionary *_transitions;
	NSMutableSet *_inputs;
	id _acceptValue;
}
- (id)initWithNFANodes:(NSSet *)nodes;
- (NSSet *)inputs;
- (NSSet *)nfaNodes;
- (id)acceptValue;
- (void)setAcceptValue:(id)value;
- (CXDFANode *)nodeForInput:(NSUInteger)input;
- (void)setTransition:(NSUInteger)input toNode:(CXDFANode *)node;
- (NSSet *)move:(NSUInteger)input;
@end
