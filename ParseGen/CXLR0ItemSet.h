//
//  CXLR0ItemSet.h
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CXLR0Item;

@interface CXLR0ItemSet : NSObject {
	NSMutableSet *_items;
}
- (void)addItem:(CXLR0Item *)item;
- (void)unionSet:(CXLR0ItemSet *)set;
- (BOOL)containsItem:(CXLR0Item *)item;
+ (CXLR0ItemSet *)set;
@end
