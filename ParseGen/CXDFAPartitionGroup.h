//
//  CXDFAPartitionGroup.h
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CXDFANode;

@interface CXDFAPartitionGroup : NSObject <NSCopying> {
	NSMutableSet *_nodes;
	NSMutableSet *_inputs;
	NSMutableSet *_nfaNodes;
}
- (NSSet *)nodes;
- (NSSet *)inputs;
- (NSSet *)nfaNodes;
- (NSUInteger)count;
- (id)acceptValue;
- (void)addNode:(CXDFANode *)node;
- (BOOL)containsNode:(CXDFANode *)node;
- (CXDFANode *)nodeForInput:(NSUInteger)input;
@end
