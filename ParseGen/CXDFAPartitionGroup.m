//
//  CXDFAPartitionGroup.m
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import "CXDFAPartitionGroup.h"
#import "CXDFANode.h"

@implementation CXDFAPartitionGroup
#pragma mark Initialization/Deallocation Methods
- (id)init {
	if (self = [super init]) {
		_nodes = [[NSMutableSet alloc] init];
		_inputs = [[NSMutableSet alloc] init];
		_nfaNodes = [[NSMutableSet alloc] init];
	}
	return self;
}
- (void)dealloc {
	[_nodes release];
	[_inputs release];
	[_nfaNodes release];
	[super dealloc];
}

#pragma mark Property Methods
- (NSSet *)nodes {
	return _nodes;
}
- (NSSet *)inputs {
	return _inputs;
}
- (NSSet *)nfaNodes {
	return _nfaNodes;
}

#pragma mark Derived Property Methods
- (NSUInteger)count {
	return [_nodes count];
}
- (id)acceptValue {
	return [[_nodes anyObject] acceptValue];
}

#pragma mark Utility Methods
- (void)addNode:(CXDFANode *)node {
	[_nodes addObject:node];
	[_inputs unionSet:[node inputs]];
	[_nfaNodes unionSet:[node nfaNodes]];
}
- (BOOL)containsNode:(CXDFANode *)node {
	return [_nodes containsObject:node];
}
- (CXDFANode *)nodeForInput:(NSUInteger)input {
	return [[_nodes anyObject] nodeForInput:input];
}

#pragma mark NSCopying Protocol Methods
- (id)copyWithZone:(NSZone *)zone {
	return [self retain];
}
@end
