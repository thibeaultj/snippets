//
//  CXDFAPartition.h
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CXDFANode;
@class CXDFAPartitionGroup;

@interface CXDFAPartition : NSObject {
	NSMutableSet *_groups;
	NSMutableDictionary *_nodeGroups;
}
- (NSSet *)groups;
- (BOOL)canAddNode:(CXDFANode *)node toSubgroup:(CXDFAPartitionGroup *)group;
- (void)addGroup:(CXDFAPartitionGroup *)group;
- (CXDFAPartitionGroup *)groupForNode:(CXDFANode *)node;
- (CXDFAPartition *)split;
+ (CXDFAPartition *)partition;
@end
