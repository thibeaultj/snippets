//
//  CXLR0Item.h
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CXProduction;
@class CXSymbol;
@class CXLR0ItemSet;

@interface CXLR0Item : NSObject <NSCopying> {
	CXProduction *_production;
	NSUInteger _marker;
}
- (id)initWithProduction:(CXProduction *)production marker:(NSUInteger)marker;
- (CXProduction *)production;
- (NSUInteger)marker;
- (CXSymbol *)elementAtMarker;
- (CXSymbol *)elementAtMarkerOffset:(NSUInteger)offset;
- (CXLR0ItemSet *)closure;
+ (CXLR0Item *)itemWithProduction:(CXProduction *)production marker:(NSUInteger)marker;
@end
