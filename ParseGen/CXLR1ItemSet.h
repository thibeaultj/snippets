//
//  CXLR1ItemSet.h
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CXLR1Item;
@class CXLR0Item;
@class CXSymbol;

@interface CXLR1ItemSet : NSObject <NSCopying> {
	NSMutableSet *_items;
	NSMutableDictionary *_gotos;
}
- (void)addItem:(CXLR1Item *)item;
- (void)unionSet:(CXLR1ItemSet *)set;
- (BOOL)containsItem:(CXLR1Item *)item;
- (NSSet *)next;
- (NSSet *)core;
- (NSSet *)terminalsForLR0:(CXLR0Item *)lr0;
- (CXLR1ItemSet *)closure;
- (CXLR1ItemSet *)gotoForSymbol:(CXSymbol *)symbol;
+ (CXLR1ItemSet *)set;
@end
