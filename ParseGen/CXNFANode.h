//
//  CXNFANode.h
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define CXTransitionEmpty						((NSUInteger)-1)

@interface CXNFANode : NSObject <NSCopying> {
	NSMutableDictionary *_transitions;
	id _acceptValue;
	NSInteger _acceptPriority;
	
	NSMutableSet *_eClosure;
}
- (NSDictionary *)transitions;
- (id)acceptValue;
- (void)setAcceptValue:(id)value;
- (NSInteger)acceptPriority;
- (void)setAcceptPriority:(NSInteger)priority;
- (void)addTransition:(NSUInteger)input toNode:(CXNFANode *)node;
- (NSSet *)inputs;
- (NSSet *)eClosure;
@end
