//
//  CXNFA.h
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "CXInputSet.h"

@class CXNFANode;

@interface CXNFA : NSObject <NSCopying> {
	NSMutableSet *_nodes;
	CXNFANode *_startNode;
	CXNFANode *_acceptNode;
}
- (id)initWithInput:(NSUInteger)input;
- (id)initWithLiteral:(NSString *)literal;
- (id)initWithInputSet:(CXInputSet *)inputSet;
- (id)initWithNodes:(NSArray *)nodes;
- (CXNFANode *)startNode;
- (void)setStartNode:(CXNFANode *)node;
- (CXNFANode *)acceptNode;
- (void)setAcceptNode:(CXNFANode *)node;
- (void)setAcceptValue:(id)value withPriority:(NSInteger)priority;
- (CXNFA *)concatenate:(CXNFA *)nfa;
- (CXNFA *)alternate:(CXNFA *)nfa;
- (CXNFA *)optionalClose;
- (CXNFA *)kleeneClose;
- (CXNFA *)positiveClose;
- (CXNFA *)rangeClose:(NSRange)r;
+ (CXNFA *)nfaFromRegularExpression:(NSString *)regex withInputSets:(NSDictionary *)inputSets mask:(CXInputSetMask)mask error:(NSError **)outError;
+ (CXNFA *)nfaFromLiteralExpression:(NSString *)literal;
@end
