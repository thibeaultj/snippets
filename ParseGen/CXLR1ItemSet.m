//
//  CXLR1ItemSet.m
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import "CXLR1ItemSet.h"
#import "CXLR0Item.h"
#import "CXLR1Item.h"
#import "CXSymbol.h"
#import "CXTerminal.h"

@implementation CXLR1ItemSet
#pragma mark Initialization/Deallocation Methods
- (id)init {
	if (self = [super init]) {
		_items = [[NSMutableSet alloc] init];
	}
	return self;
}
- (void)dealloc {
	[_items release];
	[_gotos release];
	[super dealloc];
}

#pragma mark Property Methods
- (NSSet *)items {
	return _items;
}

#pragma mark Dervied Property Methods
- (NSSet *)core {
	NSMutableSet *ret = [NSMutableSet set];
	for (CXLR1Item *item in _items)
		[ret addObject:[item lr0]];
	return ret;
}
- (NSSet *)next {
	NSMutableSet *next = [NSMutableSet set];
	for (CXLR0Item *lr0 in [self core]) 
		[next addObject:[lr0 elementAtMarker]];
	[next removeObject:[CXTerminal empty]];
	return next;
}

#pragma mark Utility Methods
- (void)addItem:(CXLR1Item *)item {
	[_items addObject:item];
}
- (void)unionSet:(CXLR1ItemSet *)set {
	[_items unionSet:[set items]];
}
- (NSSet *)terminalsForLR0:(CXLR0Item *)lr0 {
	NSMutableSet *ret = [NSMutableSet set];
	for (CXLR1Item *item in _items) {
		if ([lr0 isEqual:[item lr0]])
			[ret addObject:[item terminal]];
	}
	return ret;
}
- (BOOL)containsItem:(CXLR1Item *)item {
	return [_items containsObject:item];
}
- (CXLR1ItemSet *)closure {
	CXLR1ItemSet *set = [CXLR1ItemSet set];
	for (CXLR1Item *item in _items)
		[item closureWithSet:set];
	return set;
}
- (CXLR1ItemSet *)gotoForSymbol:(CXSymbol *)symbol {
	if (!_gotos) _gotos = [[NSMutableDictionary alloc] init];
	if (![_gotos objectForKey:symbol]) {
		CXLR1ItemSet *set = [CXLR1ItemSet set];
		for (CXLR0Item *lr0 in [self core]) {
			CXSymbol *markerElement = [lr0 elementAtMarker];
			if ([markerElement isEqual:symbol]) {
				for (CXTerminal *a in [self terminalsForLR0:lr0]) {
					CXLR0Item *nlr0 = [CXLR0Item itemWithProduction:[lr0 production] marker:[lr0 marker] + 1];
					CXLR1Item *nlr1 = [CXLR1Item itemWithLR0:nlr0 terminal:a];
					[set addItem:nlr1];
				}
			}
		}
		[_gotos setObject:[set closure] forKey:symbol];
	}
	return [_gotos objectForKey:symbol];
}

#pragma mark Class Methods
+ (CXLR1ItemSet *)set {
	return [[[CXLR1ItemSet alloc] init] autorelease];
}

#pragma mark NSCopying Protocol Methods
- (id)copyWithZone:(NSZone *)zone {
	return [self retain];
}

#pragma mark NSObject Overrides
- (NSUInteger)hash {
	NSUInteger h = 0;
	for (CXLR1Item *item in _items)
		h = h * 37 + [item hash];
	return h;
}
- (BOOL)isEqual:(id)object {
	if ([object isKindOfClass:[CXLR1ItemSet class]])
		return [self hash] == [object hash];
	return NO;
}
- (NSString *)description {
	NSString *ret = @"{";
	for (CXLR0Item *lr0 in [self core]) {
		ret = [ret stringByAppendingFormat:@"\n%@, ", lr0];
		for (CXTerminal *terminal in [self terminalsForLR0:lr0])
			ret = [ret stringByAppendingFormat:@"%@/", [terminal name]];
		ret = [ret substringToIndex:[ret length] - 1];
	}
	ret = [ret stringByAppendingString:@"\n}"];
	return ret;
}
@end
