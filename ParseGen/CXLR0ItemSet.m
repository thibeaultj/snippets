//
//  CXLR0ItemSet.m
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import "CXLR0ItemSet.h"
#import "CXLR0Item.h"

@implementation CXLR0ItemSet
#pragma mark Initialization/Deallocation Methods
- (id)init {
	if (self = [super init]) {
		_items = [[NSMutableSet alloc] init];
	}
	return self;
}
- (void)dealloc {
	[_items release];
	[super dealloc];
}

#pragma mark Property Methods
- (NSSet *)items {
	return _items;
}

#pragma mark Utility Methods
- (void)addItem:(CXLR0Item *)item {
	[_items addObject:item];
}
- (void)unionSet:(CXLR0ItemSet *)set {
	[_items unionSet:[set items]];
}
- (BOOL)containsItem:(CXLR0Item *)item {
	return [_items containsObject:item];
}

#pragma mark Class Methods
+ (CXLR0ItemSet *)set {
	return [[[CXLR0ItemSet alloc] init] autorelease];
}
@end
