//
//  CXLR0Item.m
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import "CXLR0Item.h"
#import "CXProduction.h"
#import "CXSymbol.h"
#import "CXTerminal.h"
#import "CXRule.h"
#import "CXLR0ItemSet.h"

@implementation CXLR0Item
#pragma mark Initialization/Deallocation Methods
- (id)initWithProduction:(CXProduction *)production marker:(NSUInteger)marker {
	if (self = [super init]) {
		_production = [production retain];
		_marker = marker;
	}
	return self;
}
- (void)dealloc {
	[_production release];
	[super dealloc];
}

#pragma mark Property Methods
- (CXProduction *)production {
	return _production;
}
- (NSUInteger)marker {
	return _marker;
}

#pragma mark Derived Property Methods
- (CXSymbol *)elementAtMarker {
	if (_marker == [_production count])
		return [CXTerminal empty];
	return [[_production elements] objectAtIndex:_marker];
}
- (CXSymbol *)elementAtMarkerOffset:(NSUInteger)offset {
	NSUInteger index = _marker + offset;
	if (index >= [_production count])
		return [CXTerminal empty];
	return [[_production elements] objectAtIndex:index];
}

#pragma mark Utility Methods
- (void)closureWithSet:(CXLR0ItemSet *)set {
	if (![set containsItem:self]) {
		CXSymbol *B = [self elementAtMarker];
		if ([B isKindOfClass:[CXRule class]]) {
			CXSymbol *beta = [self elementAtMarkerOffset:1];
			for (CXProduction *gamma in [(CXRule *)B productions]) {
				CXLR0Item *lr0 = [CXLR0Item itemWithProduction:gamma marker:0];
				if (![set containsItem:lr0])
					[lr0 closureWithSet:set];
				
			}
		}
	}
}
- (CXLR0ItemSet *)closure {
	CXLR0ItemSet *set = [CXLR0ItemSet set];
	[self closureWithSet:set];
	return set;
}

#pragma mark Class Methods
+ (CXLR0Item *)itemWithProduction:(CXProduction *)production marker:(NSUInteger)marker {
	return [[[CXLR0Item alloc] initWithProduction:production marker:marker] autorelease];
}

#pragma mark NSCopying Protocol Methods
- (id)copyWithZone:(NSZone *)zone {
	return [self retain];
}

#pragma mark NSObject Overrides
- (NSUInteger)hash {
	NSUInteger h = 0;
	h = h * 37 + [_production hash];
	h = h * 37 + _marker;
	return h;
}
- (BOOL)isEqual:(id)object {
	if ([object isKindOfClass:[CXLR0Item class]])
		return [self hash] == [object hash];
	return NO;
}
- (NSString *)description {
	NSString *ret = [NSString stringWithFormat:@"%@ -> ", [[_production rule] name]];
	NSUInteger i = 0;
	for (i = 0; i < [_production count]; i++) {
		if (_marker == i)
			ret = [ret stringByAppendingString:@"⋄ "];
		ret = [ret stringByAppendingFormat:@"%@ ", [[_production elementNames] objectAtIndex:i]];
	}
	if (_marker == i)
		ret = [ret stringByAppendingString:@"⋄ "];
	return ret;
}
@end
