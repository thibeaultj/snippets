//
//  CXNFA.m
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import "CXNFA.h"
#import "CXNFANode.h"
#import "CXInputSet.h"
#import "CXParserGenerator.h"
#import "CXUtility.h"

@implementation CXNFA
#pragma mark Support Methods
- (CXNFANode *)newNode {
	CXNFANode *ret = [[CXNFANode alloc] init];
	[_nodes addObject:ret];
	[ret release];
	return ret;
}
- (void)addNodes:(NSSet *)nodes {
	[_nodes unionSet:nodes];
}
- (void)removeNode:(CXNFANode *)node {
	[_nodes removeObject:node];
}
- (CXNFA *)clone {
	NSMutableDictionary *cloneNodes = [NSMutableDictionary dictionary];
	for (CXNFANode *node in _nodes) {
		CXNFANode *clone = [[CXNFANode alloc] init];
		[clone setAcceptValue:[node acceptValue]];
		[clone setAcceptPriority:[node acceptPriority]];
		[cloneNodes setObject:clone forKey:node];
		[clone release];
	}
	for (CXNFANode *node in _nodes) {
		CXNFANode *clone = [cloneNodes objectForKey:node];
		for (NSNumber *input in [node transitions]) {
			NSSet *transitions = [[node transitions] objectForKey:input];
			for (CXNFANode *tnode in transitions) {
				CXNFANode *tclone = [cloneNodes objectForKey:tnode];
				[clone addTransition:[input unsignedIntegerValue] toNode:tclone];
			}
		}
	}
	CXNFA *ret = [[CXNFA alloc] initWithNodes:[cloneNodes allValues]];
	[ret setStartNode:[cloneNodes objectForKey:_startNode]];
	[ret setAcceptNode:[cloneNodes objectForKey:_acceptNode]];
	return ret;
}
- (void)mergeNode:(CXNFANode *)from intoNode:(CXNFANode *)to {
	for (NSNumber *input in [from transitions]) 
		for (CXNFANode *node in [[from transitions] objectForKey:input]) 
			[to addTransition:[input unsignedIntegerValue] toNode:node];
	for (CXNFANode *node in _nodes) {
		for (NSNumber *input in [node transitions]) {
			NSMutableSet *transitions = [[node transitions] objectForKey:input];
			if ([transitions containsObject:from]) {
				[transitions removeObject:from];
				[transitions addObject:to];
			}
		}
	}
	if (![to acceptValue] && [from acceptValue]) {
		[to setAcceptValue:[from acceptValue]];
		[to setAcceptPriority:[from acceptPriority]];
	} else if ([to acceptValue] && [from acceptValue]) {
		if ([to acceptPriority] > [from acceptPriority]) {
			[to setAcceptValue:[from acceptValue]];
			[to setAcceptPriority:[from acceptPriority]];			
		}
	}
	[self removeNode:from];
}

#pragma mark Initialization/Deallocation Methods
- (id)init {
	if (self = [super init]) {
		_nodes = [[NSMutableSet alloc] init];
	}
	return self;
}
- (id)initWithInput:(NSUInteger)input {
	if (self = [self init]) {
		[self setStartNode:[self newNode]];
		[self setAcceptNode:[self newNode]];
		[_startNode addTransition:input toNode:_acceptNode];
	}
	return self;
}
- (id)initWithLiteral:(NSString *)literal {
	if (self = [self init]) {
		[self setStartNode:[self newNode]];
		[self setAcceptNode:[self newNode]];
		CXNFANode *currentNode = _startNode;
		NSUInteger i;
		for (i = 0; i < [literal length]; i++) {
			CXNFANode *nextNode = _acceptNode;
			if (i < ([literal length] - 1))
				nextNode = [self newNode];
			unichar c = [literal characterAtIndex:i];
			[currentNode addTransition:(NSUInteger)c toNode:nextNode];
			currentNode = nextNode;
		}
	}
	return self;
}
- (id)initWithInputSet:(CXInputSet *)inputSet {
	if (self = [self init]) {
		[self setStartNode:[self newNode]];
		[self setAcceptNode:[self newNode]];
		NSUInteger input;
		for (input = 0; input < [inputSet mask]; input++) {
			if ([inputSet containsInput:input])
				[_startNode addTransition:input toNode:_acceptNode];
		}
	}
	return self;
}
- (id)initWithNodes:(NSArray *)nodes {
	if (self = [self init]) {
		[_nodes addObjectsFromArray:nodes];
	}
	return self;
}
- (void)dealloc {
	[_nodes release];
	[_startNode release];
	[_acceptNode release];
	[super dealloc];
}

#pragma mark Property Methods
- (NSSet *)nodes {
	return _nodes;
}
- (CXNFANode *)startNode {
	return _startNode;
}
- (void)setStartNode:(CXNFANode *)node {
	[node retain];
	[_startNode release];
	_startNode = node;
}
- (CXNFANode *)acceptNode {
	return _acceptNode;
}
- (void)setAcceptNode:(CXNFANode *)node {
	[node retain];
	[_acceptNode release];
	_acceptNode = node;
}

#pragma mark Derived Property Methods
- (void)setAcceptValue:(id)value withPriority:(NSInteger)priority {
	[_acceptNode setAcceptValue:value];
	[_acceptNode setAcceptPriority:priority];
}

#pragma mark Utility Methods
- (CXNFA *)concatenate:(CXNFA *)nfa {
	CXNFA *selfClone = [self clone];
	CXNFA *nfaClone = [nfa clone];
	
	[selfClone addNodes:[nfaClone nodes]];
	[selfClone mergeNode:[nfaClone startNode] intoNode:[selfClone acceptNode]];
	[selfClone setAcceptNode:[nfaClone acceptNode]];
	
	[nfaClone release];
	return [selfClone autorelease];
}
- (CXNFA *)alternate:(CXNFA *)nfa {
	CXNFA *selfClone = [self clone];
	CXNFA *nfaClone = [nfa clone];
	
	[selfClone addNodes:[nfaClone nodes]];
	[selfClone mergeNode:[nfaClone startNode] intoNode:[selfClone startNode]];
	[selfClone mergeNode:[nfaClone acceptNode] intoNode:[selfClone acceptNode]];
	
	[nfaClone release];
	return [selfClone autorelease];
}
- (CXNFA *)optionalClose {
	CXNFA *selfClone = [self clone];
	[[selfClone startNode] addTransition:CXTransitionEmpty toNode:[selfClone acceptNode]];
	return [selfClone autorelease];
}
- (CXNFA *)kleeneClose {
	CXNFA *selfClone = [self clone];
	CXNFANode *newStart = [selfClone newNode];
	
	[newStart addTransition:CXTransitionEmpty toNode:[selfClone startNode]];
	[newStart addTransition:CXTransitionEmpty toNode:[selfClone acceptNode]];
	[[selfClone acceptNode] addTransition:CXTransitionEmpty toNode:[selfClone startNode]];
	[selfClone setStartNode:newStart];
	
	return [selfClone autorelease];
}
- (CXNFA *)positiveClose {
	return [self concatenate:[self kleeneClose]];
}
- (CXNFA *)rangeClose:(NSRange)r {
	CXNFA *ret = nil;
	NSUInteger min = r.location;
	NSUInteger max = r.location + r.length;
	if (r.length == NSNotFound)
		max = 0;
	NSUInteger i;
	for (i = 0; i < min; i++) {
		if (ret) ret = [ret concatenate:self];
		else ret = [[self clone] autorelease];
	}
	if (!max && ret) ret = [ret concatenate:[self kleeneClose]];
	else if (!max) ret = [self kleeneClose];
	for (i = min; i < max; i++) {
		if (ret) ret = [ret concatenate:[self optionalClose]];
		else ret = [self optionalClose];
	}
	return ret;
}

#pragma mark Regular Expression Parsing Methods
- (CXNFA *)closeWithExpression:(NSString *)closure used:(NSUInteger *)used error:(NSError **)outError {
	CXNFA *ret = self;
	*used = 0;
	NSUInteger index = 0;
	switch ([closure characterAtIndex:index++]) {
		case '*': ret = [self kleeneClose]; break;
		case '+': ret = [self positiveClose]; break;
		case '?': ret = [self optionalClose]; break;
		case '{': {
			BOOL closed = NO;
			while (index < [closure length] && !closed) {
				if ([closure characterAtIndex:index++] == '}')
					closed = YES;
			}
			NSString *range = [closure substringWithRange:NSMakeRange(1, index - 2)];
			NSArray *parts = [range componentsSeparatedByString:@","];
			if (closed && [parts count] && [parts count] <= 2) {
				NSUInteger min = 0;
				NSUInteger length = NSNotFound;
				min = [[[parts objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] integerValue];
				if ([parts count] > 1) {
					NSUInteger max = [[[parts objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] integerValue];
					if (max >= min) 
						length = max - min;					
				} 
				ret = [self rangeClose:NSMakeRange(min, length)];
			} else {
				ret = nil;
				if (outError) {
					NSMutableDictionary *ui = [NSMutableDictionary dictionary];
					[ui setObject:range forKey:CXGrammarErrorErrorTextKey];
					[ui setObject:[NSNumber numberWithUnsignedInteger:0] forKey:CXGrammarErrorIndexKey];
					*outError = [NSError errorWithDomain:CXGrammarErrorDomain code:CXGrammarErrorInvalidClosure userInfo:ui];
				}
			}
			break;
		}
		default:
			index--;
			break;
	}
	*used = index;
	return ret;
}
+ (NSString *)substringOfType:(unichar)type fromExpression:(NSString *)regex atIndex:(NSUInteger *)iptr error:(NSError **)outError {
	NSUInteger i = *iptr;
	BOOL insideClass = NO;
	BOOL escapedCharacter = NO;
	BOOL haveClassCharacters = NO;
	BOOL closed = NO;
	NSUInteger startIndex = i;
	NSUInteger pLevel = 1;
	NSUInteger length = [regex length];
	
	while (i < length && !closed) {
		if (escapedCharacter) --i;
		unichar c = escapedCharacter ? [CXUtility escapedCharacterFromString:regex atIndex:&i] : [regex characterAtIndex:i++];
		
		if (type == '(') {
			if (!insideClass && !escapedCharacter && c == '[') {
				haveClassCharacters = NO;
				insideClass = YES;
			} else if (insideClass && !escapedCharacter && haveClassCharacters && c == ']') {
				haveClassCharacters = NO;
				insideClass = NO;
			} else if (insideClass && !escapedCharacter && !haveClassCharacters && c != '^')
				haveClassCharacters = YES;
			if (!insideClass && !escapedCharacter && c == ')')
				pLevel--;
			else if (!insideClass && !escapedCharacter && c == '(')
				pLevel++;
			if (pLevel == 0) closed = YES;			
		} else if (type == '[') {
			if (!escapedCharacter && haveClassCharacters && c == ']') closed = YES;
			else if (!escapedCharacter && !haveClassCharacters && c != '^') haveClassCharacters = YES;
		} else if (type == '{') {
			if (!escapedCharacter && c == '}') closed = YES;
		} else if (type == '"') {
			if (!escapedCharacter && c == '"') closed = YES;
		} else if (type == '|') {
			if (!insideClass && !escapedCharacter && c == '[') {
				haveClassCharacters = NO;
				insideClass = YES;
			} else if (insideClass && !escapedCharacter && haveClassCharacters && c == ']') {
				haveClassCharacters = NO;
				insideClass = NO;
			} else if (insideClass && !escapedCharacter && !haveClassCharacters && c != '^')
				haveClassCharacters = YES;
			if (!insideClass && !escapedCharacter && c == ')')
				pLevel--;
			else if (!insideClass && !escapedCharacter && c == '(')
				pLevel++;
			if ((pLevel < 1) || (!escapedCharacter && !insideClass && !closed && pLevel == 1 && c == '|')) {
				closed = YES;
			} else if (i == length) {
				closed = YES;
				i++;
			}
		}
		
		escapedCharacter = (!escapedCharacter && c == '\\') ? YES : NO;
	}
	if (!closed) {
		if (outError) {
			NSMutableDictionary *ui = [NSMutableDictionary dictionary];
			[ui setObject:[NSNumber numberWithUnsignedInteger:i - startIndex] forKey:CXGrammarErrorIndexKey];
			[ui setObject:regex forKey:CXGrammarErrorErrorTextKey];
			*outError = [NSError errorWithDomain:CXGrammarErrorDomain code:CXGrammarErrorInvalidRegularExpression userInfo:ui];
		}
		return nil;
	}
	*iptr = type == '|' ? i - 1 : i;
	return [regex substringWithRange:NSMakeRange(startIndex, i - startIndex - 1)];	
}
+ (CXNFA *)nfaFromLiteralExpression:(NSString *)literal {
	return [[[CXNFA alloc] initWithLiteral:literal] autorelease];
}
+ (CXNFA *)nfaFromRegularExpression:(NSString *)regex withInputSets:(NSDictionary *)inputSets mask:(CXInputSetMask)mask error:(NSError **)outError {
	CXNFA *ret = nil;
	NSUInteger index = 0;
	NSUInteger length = [regex length];
	while (index < length) {
		CXNFA *t = nil;
		BOOL alternate = NO;
		switch ([regex characterAtIndex:index++]) {
			case '(': {
				NSUInteger subStart = index;
				NSString *sub = [CXNFA substringOfType:'(' fromExpression:regex atIndex:&index error:outError];
				if (sub)
					t = [CXNFA nfaFromRegularExpression:sub withInputSets:inputSets mask:mask error:outError];
//				if (t && sub) NSLog(@"Grouped t (%@) = %@", sub, t);
				if (!t && outError) {
					NSMutableDictionary *ui = [[*outError userInfo] mutableCopy];
					NSNumber *eindex = [ui objectForKey:CXGrammarErrorIndexKey];
					NSUInteger nindex = [eindex unsignedIntegerValue] + subStart;
					[ui setObject:[NSNumber numberWithUnsignedInteger:nindex] forKey:CXGrammarErrorIndexKey];
					[ui setObject:regex forKey:CXGrammarErrorRegexKey];
					*outError = [NSError errorWithDomain:[*outError domain] code:[*outError code] userInfo:ui];
					[ui release];
				}
				break;
			}
			case '[': {
				NSUInteger subStart = index;
				NSString *sub = [CXNFA substringOfType:'[' fromExpression:regex atIndex:&index error:outError];
				if (sub) {
					CXInputSet *set = nil;
					if ([sub hasPrefix:@":"]) {
						if ([sub hasSuffix:@":"]) {
							NSString *tsub = [sub substringWithRange:NSMakeRange(1, [sub length] - 2)];
							set = [inputSets objectForKey:tsub];
						}
					} else
						set = [[CXInputSet alloc] initWithSpecifier:sub mask:mask error:outError];					
					if (set) {
						t = [[[CXNFA alloc] initWithInputSet:set] autorelease];
						[set release];						
					}
				}
//				if (t && sub) NSLog(@"Set t (%@) = %@", sub, t);
				if (!t && outError) {
					NSMutableDictionary *ui = [[*outError userInfo] mutableCopy];
					NSNumber *eindex = [ui objectForKey:CXGrammarErrorIndexKey];
					NSUInteger nindex = subStart;
					if (eindex) nindex += [eindex unsignedIntegerValue];
					else if (sub) [ui setObject:sub forKey:CXGrammarErrorErrorTextKey];
					[ui setObject:[NSNumber numberWithUnsignedInteger:nindex] forKey:CXGrammarErrorIndexKey];
					[ui setObject:regex forKey:CXGrammarErrorRegexKey];
					*outError = [NSError errorWithDomain:[*outError domain] code:[*outError code] userInfo:ui];
					[ui release];
				}
				break;				
			}
/*				
			case '{': {
				NSUInteger subStart = index;
				NSString *sub = [CXNFA substringOfType:'{' fromExpression:regex atIndex:&index error:outError];
				if (sub) {
					CXInputSet *set = [inputSets objectForKey:sub];
					if (set) 
						t = [[[CXNFA alloc] initWithInputSet:set] autorelease];
					else if (outError) {
						NSMutableDictionary *ui = [NSMutableDictionary dictionary];
						[ui setObject:sub forKey:CXGrammarErrorErrorTextKey];
						[ui setObject:sub forKey:CXGrammarErrorInputSetKey];
						*outError = [NSError errorWithDomain:CXGrammarErrorDomain code:CXGrammarErrorUndefinedInputSet userInfo:ui];
					}
				} 
//				if (t && sub) NSLog(@"Predefined Set t (%@) = %@", sub, t);
				if (!t && outError) {
					NSMutableDictionary *ui = [[*outError userInfo] mutableCopy];
					[ui setObject:[NSNumber numberWithUnsignedInteger:subStart] forKey:CXGrammarErrorIndexKey];
					[ui setObject:regex forKey:CXGrammarErrorRegexKey];
					*outError = [NSError errorWithDomain:[*outError domain] code:[*outError code] userInfo:ui];
					[ui release];
				}
				break;
			}
*/
			case '|': {
				NSUInteger subStart = index;
				NSString *sub = [CXNFA substringOfType:'|' fromExpression:regex atIndex:&index error:outError];
				if (sub) {
					t = [CXNFA nfaFromRegularExpression:sub withInputSets:inputSets mask:mask error:outError];
					alternate = YES;
				}
//				if (t && sub) NSLog(@"Alternate t (%@) = %@", sub, t);
				if (!t && outError) {
					NSMutableDictionary *ui = [[*outError userInfo] mutableCopy];
					NSNumber *eindex = [ui objectForKey:CXGrammarErrorIndexKey];
					NSUInteger nindex = [eindex unsignedIntegerValue] + subStart;
					[ui setObject:[NSNumber numberWithUnsignedInteger:nindex] forKey:CXGrammarErrorIndexKey];
					[ui setObject:regex forKey:CXGrammarErrorRegexKey];
					*outError = [NSError errorWithDomain:[*outError domain] code:[*outError code] userInfo:ui];
					[ui release];
				}
				break;
			}
			case '.': {
				CXInputSet *set = [[CXInputSet alloc] initWithName:nil mask:mask];
				[set negate];
				t = [[[CXNFA alloc] initWithInputSet:set] autorelease];
				[set release];
				break;
			}
			case '$': {
				t = [CXNFA nfaFromLiteralExpression:@"\n"];
				break;
			}
			case '^': {
				t = [CXNFA nfaFromRegularExpression:@"[\\t\\n\\x20]*[\\n][\\t\\x20]*" withInputSets:inputSets mask:mask	error:outError];
				break;
			}
			default: {
				--index;
				t = [[[CXNFA alloc] initWithInput:[CXUtility escapedCharacterFromString:regex atIndex:&index]] autorelease];
				break;
			}
		}
		if (t && index < length) {
			NSUInteger closeStart = index;
			NSUInteger used = 0;
			t = [t closeWithExpression:[regex substringFromIndex:index] used:&used error:outError];
			if (t) index += used;
			else if (outError) {
				NSMutableDictionary *ui = [[*outError userInfo] mutableCopy];
				NSNumber *eindex = [ui objectForKey:CXGrammarErrorIndexKey];
				NSUInteger nindex = [eindex unsignedIntegerValue] + closeStart;
				[ui setObject:[NSNumber numberWithUnsignedInteger:nindex] forKey:CXGrammarErrorIndexKey];
				[ui setObject:regex forKey:CXGrammarErrorRegexKey];		
				*outError = [NSError errorWithDomain:[*outError domain] code:[*outError code] userInfo:ui];
				[ui release];
			}
		}
		if (t && !ret) ret = t;
		else if (t && alternate) ret = [ret alternate:t];
		else if (t) ret = [ret concatenate:t];
		else return nil;
//		NSLog(@"ret = %@", ret);
	}
	return ret;
}

#pragma mark NSCopying Protocol Methods
- (id)copyWithZone:(NSZone *)zone {
	return [self retain];
}

#pragma mark NSObject Overrides
- (NSString *)description {
	NSArray *nodeArray = [_nodes allObjects];
	NSUInteger start = [nodeArray indexOfObject:_startNode];
	NSUInteger accept = [nodeArray indexOfObject:_acceptNode];
	NSString *ret = [NSString stringWithFormat:@"NFA (%d nodes): start=%d; accept=%d\n", [_nodes count], start, accept];
	for (CXNFANode *node in nodeArray) {
		NSMutableDictionary *targets = [NSMutableDictionary dictionary];
		NSDictionary *transitions = [node transitions];
		for (NSNumber *num in transitions) {
			NSUInteger input = [num unsignedIntegerValue];
			NSMutableSet *set = [transitions objectForKey:num];
			for (CXNFANode *tnode in set) {
				NSMutableIndexSet *is = [targets objectForKey:tnode];
				if (!is) {
					is = [NSMutableIndexSet indexSet];
					[targets setObject:is forKey:tnode];
				}
				[is addIndex:input];
			}
		}
		NSUInteger nindex = [nodeArray indexOfObject:node];
		if (![targets count])
			ret = [ret stringByAppendingFormat:@"Node %d: No Transitions\n", nindex];
		for (CXNFANode *tnode in targets) {
			NSUInteger index = [nodeArray indexOfObject:tnode];
			NSIndexSet *is = [targets objectForKey:tnode];
			NSMutableArray *ranges = [NSMutableArray array];
			NSInteger i;
			NSRange r = NSMakeRange(NSNotFound, 0);
			for (i = -2; i < 128; i++) {
				if ([is containsIndex:i]) {
					if (r.location == NSNotFound)
						r.location = i;
					r.length++;
				} else if (r.location != NSNotFound) {
					[ranges addObject:[NSValue valueWithRange:r]];
					r.location = NSNotFound;
					r.length = 0;
				}
			}
			if (r.location != NSNotFound && r.length)
				[ranges addObject:[NSValue valueWithRange:r]];
			ret = [ret stringByAppendingFormat:@"Node %d ", nindex];
			for (NSValue *ra in ranges) {
				NSRange ran = [ra rangeValue];
				if (ran.length == 1) ret = [ret stringByAppendingFormat:@"[%d]", ran.location];
				else ret = [ret stringByAppendingFormat:@"[%d-%d]", ran.location, ran.location + ran.length - 1];
			}
			ret = [ret stringByAppendingFormat:@" -> %d\n", index];
		}
	}
	return ret;
}
@end
