//
//  CXLR1Item.h
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CXLR0Item;
@class CXTerminal;
@class CXSymbol;
@class CXLR1ItemSet;

@interface CXLR1Item : NSObject <NSCopying> {
	CXLR0Item *_lr0;
	CXTerminal *_terminal;
}
- (id)initWithLR0Item:(CXLR0Item *)item terminal:(CXTerminal *)terminal;
- (CXLR0Item *)lr0;
- (CXTerminal *)terminal;
- (CXSymbol *)elementAtMarker;
- (CXSymbol *)elementAtMarkerOffset:(NSUInteger)offset;
- (void)closureWithSet:(CXLR1ItemSet *)set;
- (CXLR1ItemSet *)closure;
+ (CXLR1Item *)itemWithLR0:(CXLR0Item *)lr0 terminal:(CXTerminal *)terminal;
@end
