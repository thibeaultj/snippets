//
//  CXDFA.h
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CXDFANode;

@interface CXDFA : NSObject {
	NSMutableSet *_nodes;
	CXDFANode *_startNode;
}
- (id)initWithNFASet:(NSSet *)nfas;
- (CXDFANode *)startNode;
- (void)setStartNode:(CXDFANode *)node;
- (CXDFA *)optimize;
- (NSSet *)nodes;
@end
