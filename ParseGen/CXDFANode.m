//
//  CXDFANode.m
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import "CXDFANode.h"
#import "CXNFANode.h"
#import "CXTerminal.h"
#import "CXConflictOptions.h"

@implementation CXDFANode
#pragma mark Initialization/Deallocation Methods
- (id)initWithNFANodes:(NSSet *)nodes {
	if (self = [super init]) {
		_nfaNodes = [nodes retain];
		_transitions = [[NSMutableDictionary alloc] init];
		_inputs = [[NSMutableSet alloc] init];
		_acceptValue = nil;
		NSInteger priority = CXPriorityMinimum;
		for (CXNFANode *node in nodes) {
			[_inputs unionSet:[node inputs]];
			if ([node acceptValue]) {
				if (!_acceptValue || [node acceptPriority] < priority) {
					[self setAcceptValue:[node acceptValue]];
					priority = [node acceptPriority];
				}
			}
		}
		[_inputs removeObject:[NSNumber numberWithUnsignedInteger:CXTransitionEmpty]];
	}
	return self;
}
- (void)dealloc {
	[_nfaNodes release];
	[_transitions release];
	[_inputs release];
	[_acceptValue release];
	[super dealloc];
}

#pragma mark Property Methods
- (NSSet *)inputs {
	return _inputs;
}
- (NSSet *)nfaNodes {
	return _nfaNodes;
}
- (id)acceptValue {
	return _acceptValue;
}
- (void)setAcceptValue:(id)value {
	[value retain];
	[_acceptValue release];
	_acceptValue = value;
}

#pragma mark Utility Methods
- (CXDFANode *)nodeForInput:(NSUInteger)input {
	return [_transitions objectForKey:[NSNumber numberWithUnsignedInteger:input]];
}
- (void)setTransition:(NSUInteger)input toNode:(CXDFANode *)node {
	[_transitions setObject:node forKey:[NSNumber numberWithUnsignedInteger:input]];
}
- (NSSet *)move:(NSUInteger)input {
	NSMutableSet *set = [NSMutableSet set];
	for (CXNFANode *node in _nfaNodes) {
		for (CXNFANode *target in [[node transitions] objectForKey:[NSNumber numberWithUnsignedInteger:input]])
			[set unionSet:[target eClosure]];
	}
	return set;
}

#pragma mark NSCopying Protocol Methods
- (id)copyWithZone:(NSZone *)zone {
	return [self retain];
}

#pragma mark NSObject Overrides
- (NSUInteger)hash {
	return [_nfaNodes hash];
}
- (BOOL)isEqual:(id)other {
	if ([other isKindOfClass:[CXDFANode class]])
		return [self hash] == [other hash];
	else if ([other isKindOfClass:[NSSet class]])
		return [self hash] == [other hash];
	return NO;
}
@end
