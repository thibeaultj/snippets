//
//  CXDFA.m
//  CodeX Studio
//
//  Created by Jeffrey Thibeault on 6/19/10.
//  Copyright 2010 Tabbycat Software. All rights reserved.
//

#import "CXDFA.h"
#import "CXDFANode.h"
#import "CXNFANode.h"
#import "CXNFA.h"
#import "CXUtility.h"
#import "CXDFAPartition.h"
#import "CXDFAPartitionGroup.h"

@implementation CXDFA
#pragma mark Support Methods
- (CXDFANode *)newNode:(NSSet *)nfaNodes {
	CXDFANode *node = [[CXDFANode alloc] initWithNFANodes:nfaNodes];
	[_nodes addObject:node];
	[node release];
	return node;
}
- (void)addNode:(CXDFANode *)node {
	[_nodes addObject:node];
}

#pragma mark Initialization/Deallocation Methods
- (id)init {
	if (self = [super init]) {
		_nodes = [[NSMutableSet alloc] init];
	}
	return self;
}
- (id)initWithNFASet:(NSSet *)nfas {
	if (self = [self init]) {
		CXNFANode *s0 = [[CXNFANode alloc] init];
		for (CXNFA *nfa in nfas)
			[s0 addTransition:CXTransitionEmpty toNode:[nfa startNode]];
		
		[self setStartNode:[self newNode:[s0 eClosure]]];
		CXStack *unmarked = [CXStack stack];
		[unmarked push:_startNode];
		CXDFANode *node;
		while (node = [unmarked pop]) {
			for (NSNumber *input in [node inputs]) {
				NSSet *u = [node move:[input unsignedIntegerValue]];
				CXDFANode *foundNode = nil;
				foundNode = [_nodes member:u];
/*				
				for (CXDFANode *tnode in _nodes) {
					if ([[tnode nfaNodes] isEqualToSet:u]) {
						foundNode = tnode;
						break;
					}
				}
*/
				if (!foundNode) {
					foundNode = [self newNode:u];
					[unmarked push:foundNode];
				}
				[node setTransition:[input unsignedIntegerValue] toNode:foundNode];
			}
		}
		
		[s0 release];
	}
	return self;
}
- (void)dealloc {
	[_nodes release];
	[_startNode release];
	[super dealloc];
}

#pragma mark Property Methods
- (CXDFANode *)startNode {
	return _startNode;
}
- (void)setStartNode:(CXDFANode *)node {
	[node retain];
	[_startNode release];
	_startNode = node;
}
- (NSSet *)nodes {
	return _nodes;
}

#pragma mark Utility Methods
- (CXDFA *)optimize {
	CXDFAPartition *p = [CXDFAPartition partition];
	NSMutableDictionary *f = [NSMutableDictionary dictionary];
	for (CXDFANode *node in _nodes) {
		id key = [node acceptValue];
		if (!key) key = [NSNull null];
		CXDFAPartitionGroup *pg = [f objectForKey:key];
		if (!pg) {
			pg = [[CXDFAPartitionGroup alloc] init];
			[f setObject:pg forKey:key];
			[pg release];
		}
		[pg addNode:node];
	}
	for (id key in f)
		[p addGroup:[f objectForKey:key]];
	
	CXDFAPartition *split = p;
	do {
		p = split;
		split = [p split];
	} while (split != p);
	
	CXDFANode *startNode = nil;
	NSMutableDictionary *groupMap = [NSMutableDictionary dictionary];
	for (CXDFAPartitionGroup *group in [p groups]) {
		CXDFANode *node = [[CXDFANode alloc] initWithNFANodes:[group nfaNodes]];
		[node setAcceptValue:[group acceptValue]];
		if ([group containsNode:_startNode])
			startNode = node;
		[groupMap setObject:node forKey:group];
	}
	for (CXDFAPartitionGroup *group in groupMap) {
		CXDFANode *node = [groupMap objectForKey:group];
		for (NSNumber *i in [group inputs]) {
			NSUInteger input = [i unsignedIntegerValue];
			CXDFANode *target = [group nodeForInput:input];
			CXDFAPartitionGroup *targetGroup = [p groupForNode:target];
			CXDFANode *newTarget = [groupMap objectForKey:targetGroup];
			[node setTransition:input toNode:newTarget];
		}
	}
	CXDFA *dfa = [[CXDFA alloc] init];
	for (CXDFAPartitionGroup *g in groupMap)
		[dfa addNode:[groupMap objectForKey:g]];
	[dfa setStartNode:startNode];
	return [dfa autorelease];
}
@end
