# Snippets #

This repository is a small collection of code snippets/chunks from various projects.

## ParseGen ##
This chunk is from a project that was a port and evolution of an pure-C version of an experimental Parser Generator/Compiler, which we originally built in the late 90s/early 00s, and then had wanted to move onto Mac and mobile platforms around the time of the ipad introduction.  The chunk covers building fairly optimized lex tables from near canonical regex, and shows a state of code where the regexes were rule-parsed.  A later version used itself as a tool to build a self-generated parser to examine the regex.